FROM openjdk:17

WORKDIR /app

COPY target/member-service-0.0.1-SNAPSHOT.jar /app/member-service-0.0.1.jar

EXPOSE 8083

CMD ["java", "-jar", "member-service-0.0.1.jar"]