package com.book.memberservice.controller;

import com.book.memberservice.exception.DuplicateException;
import com.book.memberservice.model.BaseResponse;
import com.book.memberservice.model.Member;
import com.book.memberservice.model.Peminjaman;
import com.book.memberservice.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/members")
@Slf4j
public class MemberController {
    private static final Logger logger = LoggerFactory.getLogger(MemberController.class);

    private final MemberService memberService;

    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @PostMapping
    public ResponseEntity<BaseResponse<Member>> addMember(@Validated @RequestBody Member member) {
        BaseResponse<Member> response;
        log.info("Incoming request in member controller");

        try {
            Member createdMember = memberService.createMember(member);
            response = new BaseResponse<>("ok", HttpStatus.OK.value(), createdMember, "");
            return ResponseEntity.ok().body(response);
        } catch (DuplicateException e) {
            response = new BaseResponse<>("error", HttpStatus.BAD_REQUEST.value(), null, e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Member>> getMemberByid(@PathVariable Long id) {
        BaseResponse<Member> response;
        try {
            Member member = memberService.getMemberById(id);

            if (member.getId() != null){
                response = new BaseResponse<>("ok", HttpStatus.OK.value(), member,"");
            }else {
                response = new BaseResponse<>("error", HttpStatus.BAD_REQUEST.value(), null, "User doesn't Exists");
            }

            return ResponseEntity.ok().body(response);

        } catch (Exception e) {
            response = new BaseResponse<>("error", HttpStatus.BAD_REQUEST.value(), null, "User doesn't Exists");
            return ResponseEntity.badRequest().body(response);

        }

    }

    @GetMapping
    public ResponseEntity<BaseResponse<List<Member>>> getAllMembers() {

        BaseResponse<List<Member>> response;
        try {
            List<Member> members = memberService.getAllMembers();
            response = new BaseResponse<>("ok", HttpStatus.OK.value(), members,"");
            return ResponseEntity.ok().body(response);

        } catch (Exception e) {
            response = new BaseResponse<>("error", HttpStatus.BAD_REQUEST.value(), null, "member doesn't Exists");
            return ResponseEntity.badRequest().build();

        }
    }

    @PostMapping("/{id}")
    public ResponseEntity<BaseResponse<Member>> updateMember(@PathVariable Long id, @RequestBody Member member) {


        BaseResponse<Member> response;
        try {
            member.setId(id);
            memberService.updateMember(member);
            Member memberData = memberService.getMemberById(member.getId());
            response = new BaseResponse<>("success", HttpStatus.OK.value(), memberData, "Success Update Member");
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            response = new BaseResponse<>("error", HttpStatus.BAD_REQUEST.value(), null, "member doesn't Exists");
            return ResponseEntity.badRequest().build();
        }

    }
    @GetMapping("/deleteMember/{id}")
    public ResponseEntity<BaseResponse<Void>> deleteMember(@PathVariable Long id) {
        BaseResponse<Void> response;

        try {
            memberService.deleteMember(id);
            response = new BaseResponse<>("Success", 200, null, "Member deleted successfully.");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response = new BaseResponse<>("Error", 400, null, "Failed to delete member.");
            return ResponseEntity.badRequest().body(response);
        }
    }
}