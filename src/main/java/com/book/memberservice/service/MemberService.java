package com.book.memberservice.service;

import com.book.memberservice.controller.MemberController;
import com.book.memberservice.exception.DuplicateException;
import com.book.memberservice.mapper.MemberMapper;
import com.book.memberservice.model.Member;
import com.book.memberservice.model.Peminjaman;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@Service
public class MemberService {
    private static final Logger log = LoggerFactory.getLogger(MemberService.class);



    private final MemberMapper memberMapper;


    @Autowired
    public MemberService(MemberMapper memberMapper) {
        this.memberMapper = memberMapper;
    }


    public List<Member> getAllMembers() {
        return memberMapper.getAllMembers();
    }

    public Member getMemberById(Long id) {
        return memberMapper.getMemberById(id);
    }

    public Member createMember(Member member) throws DuplicateException {
        log.info("Attempting to create member: {}", member);

        String email = member.getEmail();
        Member existingMember = memberMapper.findByEmail(email);

        if (existingMember != null) {
            log.error("Email already exists: {}", email);
            throw new DuplicateException("Email already exists: " + email);
        }

        memberMapper.insertMember(member);
        log.info("Member created successfully: {}", member.getEmail());
        return member;
    }
    public void updateMember(Member member) {
        Member getMemberById = memberMapper.getMemberById(member.getId());
        if(getMemberById.getId()!=null){
            memberMapper.updateMember(member);

        }
    }

    public void deleteMember(Long id) {
        Member member =  memberMapper.getMemberById(id);
        if(member.getId() != null){
            memberMapper.deleteMember(id);
        }


    }


}
