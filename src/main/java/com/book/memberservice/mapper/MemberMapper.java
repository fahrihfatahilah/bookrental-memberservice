package com.book.memberservice.mapper;

import com.book.memberservice.model.Member;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface MemberMapper {

    @Select("SELECT * FROM member")
    List<Member> getAllMembers();

    @Select("SELECT * FROM member WHERE id = #{id}")
    Member getMemberById(Long id);

    @Insert("INSERT INTO member (name, email) VALUES (#{name}, #{email})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertMember(Member member);

    @Update("UPDATE member SET name = #{name}, email = #{email} WHERE id = #{id}")
    void updateMember(Member member);

    @Delete("DELETE FROM member WHERE id = #{id}")
    void deleteMember(Long id);

    @Select("SELECT * FROM member WHERE email = #{email}")
    Member findByEmail(@Param("email") String email);
}