package com.book.memberservice.exception;

public class DuplicateException extends Exception {
    public DuplicateException(String message) {
        super(message);
    }
}