package com.book.memberservice.model;


import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Table(name = "peminjaman")
public class Peminjaman {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "member_id")
    private Long memberId;

    @Column(name = "book_id")
    private Long bookId;

    @Column(name = "borrowing_date")
    private Date borrowingDate;

    @Column(name = "return_date")
    private Date returnDate;

    // Constructors, getters, and setters

    // Assuming you have a Many-to-One relationship with Member entity
    @ManyToOne
    @JoinColumn(name = "member_id", referencedColumnName = "id", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "peminjaman_ibfk_1"))
    private Member member;

    // Assuming you have a Many-to-One relationship with Book entity
    @ManyToOne
    @JoinColumn(name = "book_id", referencedColumnName = "id", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "peminjaman_ibfk_2"))
    private Book book;

    // Constructors, getters, and setters

}